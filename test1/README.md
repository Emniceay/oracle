# 实验1：SQL语句的执行计划分析与优化指导

    班级：软件工程4班 学号：202010414422 姓名：杨嘉良

## 实验目的

    分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

    数据库是pdborcl，用户是sys和hr

## 实验内容

    - 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
    - 设计查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

## 实验过程
    计算每个部门中的每个经理的平均薪资，并将结果按照平均薪资降序排列
- 查询1：

```SQL
SELECT D.DEPARTMENT_NAME, E.MANAGER_ID, AVG(E.SALARY) AS AVG_SALARY 
FROM EMPLOYEES E JOIN DEPARTMENTS D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID 
WHERE E.MANAGER_ID IS NOT NULL GROUP BY D.DEPARTMENT_NAME, E.MANAGER_ID ORDER BY AVG_SALARY DESC;


输出结果：
DEPARTMENT_NAME 	       MANAGER_ID AVG_SALARY
------------------------------ ---------- ----------
Executive			      100      17000
Marketing			      100      13000
Sales				      100      12200
Accounting			      101      12008
Finance 			      101      12008
Purchasing			      100      11000
Public Relations		      101      10000
IT				      102	9000
Sales				      148	8650
Sales				      149	8600
Sales				      145	8500

DEPARTMENT_NAME 	       MANAGER_ID AVG_SALARY
------------------------------ ---------- ----------
Sales				      146	8500
Accounting			      205	8300
Finance 			      108	7920
Sales				      147 7766.66667
Shipping			      100	7280
Human Resources 		      101	6500
Marketing			      201	6000
IT				      103	4950
Administration			      101	4400
Shipping			      123     3237.5
Shipping			      121	3175

DEPARTMENT_NAME 	       MANAGER_ID AVG_SALARY
------------------------------ ---------- ----------
Shipping			      122	2950
Shipping			      124	2875
Purchasing			      114	2780
Shipping			      120     2762.5

已选择 26 行。

Predicate Information (identified by operation id):
---------------------------------------------------

   6 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
       filter("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
   7 - filter("E"."MANAGER_ID" IS NOT NULL)


统计信息
----------------------------------------------------------
	573  recursive calls
	  6  db block gets
	917  consistent gets
	  0  physical reads
	  0  redo size
       1450  bytes sent via SQL*Net to client
	619  bytes received via SQL*Net from client
	  3  SQL*Net roundtrips to/from client
	 64  sorts (memory)
	  0  sorts (disk)
	 26  rows processed


```
查询1涉及两个表：employees和departments，在查询中使用了join关键字来将两个表连接到一起。使用where子句过滤出具有经理ID的员工，使用group by函数来将查询结果分组，以便根据每个部门和经理计算其平均薪资。最终，通过order by聚合函数按平均薪资降序排序结果。该查询是在员工和部门表之间建立关系，并从中获取和汇总数据，同时也保证了查询结果的正确性及完整性。

- 查询2：

```SQL
SELECT DEPARTMENT_NAME, MANAGER_ID, AVG(SALARY) AS AVG_SALARY 
FROM ( 
  SELECT D.DEPARTMENT_NAME, E.MANAGER_ID, E.SALARY 
  FROM EMPLOYEES E JOIN DEPARTMENTS D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID 
  WHERE E.MANAGER_ID IS NOT NULL AND E.SALARY = ( SELECT MAX(SALARY) 
  FROM EMPLOYEES 
  WHERE MANAGER_ID = E.MANAGER_ID AND DEPARTMENT_ID = E.DEPARTMENT_ID ) ) T GROUP BY DEPARTMENT_NAME, MANAGER_ID ORDER BY AVG_SALARY DESC;

  输出结果：
  Predicate Information (identified by operation id):
---------------------------------------------------

   3 - access("E"."SALARY"="MAX(SALARY)" AND "ITEM_1"="E"."MANAGER_ID" AND
	      "ITEM_2"="E"."DEPARTMENT_ID")
   7 - access("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
       filter("E"."DEPARTMENT_ID"="D"."DEPARTMENT_ID")
   8 - filter("E"."MANAGER_ID" IS NOT NULL)
  10 - filter(MAX("SALARY")>0)


统计信息
----------------------------------------------------------
       1307  recursive calls
	 94  db block gets
       2007  consistent gets
	  2  physical reads
      13140  redo size
       1406  bytes sent via SQL*Net to client
	619  bytes received via SQL*Net from client
	  3  SQL*Net roundtrips to/from client
	159  sorts (memory)
	  0  sorts (disk)
	 26  rows processed
```
查询2和查询1实现了相同的查询，该查询使用了内部嵌套的子查询方式。在子查询(SELECT MAX(SALARY))中，分别查询出每个部门和经理的最高薪资值。最外层查询按部门和经理分组，并计算出每个分组的平均工资。通过此方式可以避免使用窗口函数或其他高级SQL功能，并且代码相对简单。不过由于这种方式中嵌套的查询语句较多，可能会导致性能问题或复杂度高的问题，而且当数据量变得非常大时，性能下降较明显。 
## SQL优化详细指导：
- 查询1：  
  GENERAL INFORMATION SECTION
-------------------------------------------------------------------------------
Tuning Task Name   : staName81038
Tuning Task Owner  : HR
Tuning Task ID     : 11
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_31
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/20/2023 10:52:51
Completed at       : 03/20/2023 10:52:52

-------------------------------------------------------------------------------
Schema Name   : HR
Container Name: PDBORCL
SQL ID        : g9j6h3jrvkxkt
SQL Text      : SELECT D.DEPARTMENT_NAME, E.MANAGER_ID, AVG(E.SALARY) AS
                AVG_SALARY FROM EMPLOYEES E JOIN DEPARTMENTS D ON
                E.DEPARTMENT_ID = D.DEPARTMENT_ID WHERE E.MANAGER_ID IS NOT
                NULL GROUP BY D.DEPARTMENT_NAME, E.MANAGER_ID ORDER BY
                AVG_SALARY DESC

-------------------------------------------------------------------------------
There are no recommendations to improve the statement.（没有任何改进声明的建议。）

- 查询2：
GENERAL INFORMATION SECTION
-------------------------------------------------------------------------------
Tuning Task Name   : staName55348
Tuning Task Owner  : HR
Tuning Task ID     : 21
Workload Type      : Single SQL Statement
Execution Count    : 1
Current Execution  : EXEC_41
Execution Type     : TUNE SQL
Scope              : COMPREHENSIVE
Time Limit(seconds): 1800
Completion Status  : COMPLETED
Started at         : 03/20/2023 11:39:32
Completed at       : 03/20/2023 11:39:33

-------------------------------------------------------------------------------
Schema Name   : HR
Container Name: PDBORCL
SQL ID        : 00ksm35kf84fg
SQL Text      : SELECT DEPARTMENT_NAME, MANAGER_ID, AVG(SALARY) AS AVG_SALARY
                FROM ( 
                  SELECT D.DEPARTMENT_NAME, E.MANAGER_ID, E.SALARY 
                  FROM EMPLOYEES E JOIN DEPARTMENTS D ON E.DEPARTMENT_ID =
                D.DEPARTMENT_ID 
                  WHERE E.MANAGER_ID IS NOT NULL AND E.SALARY = ( SELECT
                MAX(SALARY) 
                  FROM EMPLOYEES 
                  WHERE MANAGER_ID = E.MANAGER_ID AND DEPARTMENT_ID =
                E.DEPARTMENT_ID ) ) T GROUP BY DEPARTMENT_NAME, MANAGER_ID
                ORDER BY AVG_SALARY DESC

-------------------------------------------------------------------------------
There are no recommendations to improve the statement.（没有任何改进声明的建议。）
