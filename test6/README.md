﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计
    学号：202010414422 姓名：杨嘉良
- ## 设计权限及用户分配方案。
  - 1.pdborcl插接式数据中创建一个新的本地角色shop_role，该角色包含shopnect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有shop_role的用户就同时拥有这三种权限
  - 2.创建角色之后，再创建用户sale，给用户分配表空间，设置限额为3*1024M，授予shop_role角色。  
  - 3.最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。
```sql
  $ sqlplus system/123@pdborcl
  CREATE ROLE shop_role;
  GRANT connect,resource,CREATE VIEW TO shop_role;
  CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
  ALTER USER sale default TABLESPACE "USERS";
  ALTER USER sale QUOTA 3G ON users;
  GRANT shop_role TO sale;
--分配权限
ALTER USER sale QUOTA UNLIMITED ON SALE_DATA;
ALTER USER sale QUOTA UNLIMITED ON SALE_INDEX;
GRANT CREATE PROCEDURE, CREATE TYPE, CREATE SEQUENCE, CREATE TRIGGER, CREATE VIEW TO sale;
```
![](createusers.png)
- ## 表及表空间设计方案。
### 表空间设计方案

为了保证数据库的性能和可维护性，设计了如下两个表空间：

1. 数据表空间

   - 表空间名称： SALE_DATA
   - 数据文件： sale_data01.dbf
   - 数据文件大小：3G
   - 数据块大小：8KB

2. 索引表空间

   - 表空间名称： SALE_INDEX
   - 数据文件： sale_index01.dbf
   - 数据文件大小：3G
   - 数据块大小：8KB

这样设计的原因是因为数据表空间主要用来存储数据表，而索引表空间主要用来存储表的索引，这种设计可以使数据库的查询性能得到有效的提升。
```sql
-- 创建SALE_DATA数据表空间
CREATE TABLESPACE SALE_DATA
DATAFILE '/home/oracle/app/oracle/oradata/ORCL/sale_data01.dbf'
SIZE 3G
AUTOEXTEND ON
NEXT 50M
MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO
BLOCKSIZE 8K;

-- 创建SALE_INDEX索引表空间
CREATE TABLESPACE SALE_INDEX
DATAFILE '/home/oracle/app/oracle/oradata/ORCL/sale_index01.dbf'
SIZE 3G
AUTOEXTEND ON
NEXT 50M
MAXSIZE UNLIMITED
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO
BLOCKSIZE 8K;
```
![](CREATETABLESPACE1.png)
![](CREATETABLESPACE2.png)
### 表设计方案

为了模拟商品销售系统的功能，设计了以下4张表：

1. 商品表（goods）

   - 字段列表：

     | 字段名      | 类型      | 描述         |
     | ----------- | --------- | ------------ |
     | id          | int       | 商品ID       |
     | name        | varchar   | 商品名称     |
     | price       | decimal   | 商品价格     |
     | stock       | int       | 商品库存数量 |
     | category_id | int       | 商品分类ID   |

   - 索引列表：

     | 索引名         | 索引类型 | 索引字段    |
     | -------------- | -------- | ----------- |
     | goods_pk       | 主键索引 | id          |
     | goods_price_ix | 普通索引 | price       |
     | goods_cate_ix  | 普通索引 | category_id |

2. 分类表（category）

   - 字段列表：

     | 字段名 | 类型    | 描述     |
     | ------ | ------- | -------- |
     | id     | int     | 分类ID   |
     | name   | varchar | 分类名称 |

   - 索引列表：

     | 索引名            | 索引类型 | 索引字段 |
     | ----------------- | -------- | -------- |
     | cate_pk           | 主键索引 | id       |
     | cate_name_ix      | 普通索引 | name     |

3. 用户表（user）

   - 字段列表：

     | 字段名    | 类型    | 描述     |
     | --------- | ------- | -------- |
     | id        | int     | 用户ID   |
     | username  | varchar | 用户名   |
     | password  | varchar | 用户密码 |
     | email     | varchar | 用户邮箱 |

   - 索引列表：

     | 索引名        | 索引类型 | 索引字段 |
     | ------------- | -------- | --------|
     | user_pk       | 主键索引 | id       |
     | user_name_ix  | 普通索引 | username |

4. 订单表（order）

   - 字段列表：

     | 字段名     | 类型          | 描述           |
     | ---------- | ------------- | --------------|
     | id         | int           | 订单ID         |
     | user_id    | int           | 用户ID         |
     | order_time | datetime      | 下单时间       |
     | pay_time   | datetime      | 支付时间       |
     | total      | decimal(10,2) | 订单总金额     |
     | status     | varchar(20)   | 订单状态（已支付或未支付） |

   - 索引列表：

     | 索引名             |索引类型 | 索引字段 |
     |-------------------| -------- | -------- |
     | order_pk           | 主键索引 | id        |
     | order_user_ix      | 普通索引 | user_id   |
     | order_time_ix      | 普通索引 | order_time|
     | order_pay_ix       | 普通索引 | pay_time  |
### 创建表
```sql
-- 创建分类表（category）
CREATE TABLE category (
  id   NUMBER(10) PRIMARY KEY,
  name VARCHAR2(50) NOT NULL
)
TABLESPACE SALE_DATA;
-- 创建商品表（goods）
CREATE TABLE goods (
  id          NUMBER(10) PRIMARY KEY,
  name        VARCHAR2(50) NOT NULL,
  price       NUMBER(10,2) NOT NULL,
  stock       NUMBER(10) NOT NULL,
  category_id NUMBER(10) NOT NULL,
  CONSTRAINT fk_goods_category
    FOREIGN KEY (category_id)
    REFERENCES category (id)
)
TABLESPACE SALE_DATA;
-- 创建用户表（users）
CREATE TABLE users (
  id       NUMBER(10) PRIMARY KEY,
  username VARCHAR2(50) NOT NULL,
  password VARCHAR2(50) NOT NULL,
  email    VARCHAR2(50) NOT NULL
)
TABLESPACE SALE_DATA;

-- 创建订单表（orders）
CREATE TABLE orders (
  id          NUMBER(10) PRIMARY KEY,
  user_id     NUMBER(10) NOT NULL,
  order_time  DATE NOT NULL,
  pay_time    DATE,
  total       NUMBER(10,2) NOT NULL,
  status      VARCHAR2(20) NOT NULL,
  CONSTRAINT fk_orders_user
    FOREIGN KEY (user_id)
    REFERENCES users (id)
)
TABLESPACE SALE_DATA;
```
![](createtable.png)
接下来，使用以下命令在对应的表空间中创建索引：

```sql
-- 在SALE_INDEX表空间中创建goods表的索引
CREATE INDEX goods_price_ix ON goods (price) TABLESPACE SALE_INDEX;
CREATE INDEX goods_cate_ix ON goods (category_id) TABLESPACE SALE_INDEX;

-- 在SALE_INDEX表空间中创建category表的索引
CREATE INDEX cate_name_ix ON category (name) TABLESPACE SALE_INDEX;

-- 在SALE_INDEX表空间中创建orders表的索引
CREATE INDEX order_user_ix ON orders (user_id) TABLESPACE SALE_INDEX;
CREATE INDEX order_time_ix ON orders (order_time) TABLESPACE SALE_INDEX;
CREATE INDEX order_pay_ix ON orders (pay_time) TABLESPACE SALE_INDEX;
```
![](createindex.png)
拟插入要求数量的数据：
为了模拟10万条数据，我们可以使用循环插入的方式，使用dbms_random包生成随机数据。
```sql
-- 向category表中插入随机数据
DECLARE
  v_name VARCHAR2(100);
BEGIN
  FOR i IN 1..100 LOOP
    v_name := 'Category ' || i;
    INSERT INTO category(id, name)
    VALUES (i, v_name);
  END LOOP;
END;
/

-- 向goods表中插入随机数据
DECLARE
  v_name       VARCHAR2(100);
BEGIN
  FOR i IN 1..100000 LOOP
    v_name := 'Product ' || i;
    INSERT INTO goods(id, name, price, stock, category_id)
    VALUES (i, v_name, round(dbms_random.value(1, 10000), 2), ROUND(dbms_random.value(0, 1000)), trunc(dbms_random.value(1, 100)));
  END LOOP;
END;
/

-- 向users表中插入随机数据
DECLARE
  v_username   VARCHAR2(100);
  v_email      VARCHAR2(100);
BEGIN
  FOR i IN 1..1000 LOOP
    v_username := 'User ' || i;
    v_email    := 'user' || i || '@example.com';
    INSERT INTO users(id, username, password, email)
    VALUES (i, v_username, 'password', v_email);
  END LOOP;
END;
/

-- 向orders表中插入随机数据
DECLARE
  v_user_id    NUMBER(10);
  v_total      NUMBER(10,2);
  v_status     VARCHAR2(20);
BEGIN
  FOR i IN 1..1000 LOOP
    v_user_id := TRUNC(DBMS_RANDOM.VALUE(1, 1000));
    v_total   := ROUND(DBMS_RANDOM.VALUE(1, 10000), 2);
    v_status  := CASE WHEN MOD(i, 2) = 0 THEN 'paid' ELSE 'unpaid' END;
    INSERT INTO orders(id, user_id, order_time, pay_time, total, status)
    VALUES (i, v_user_id, SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 100)), SYSDATE - TRUNC(DBMS_RANDOM.VALUE(1, 100)), v_total, v_status);
  END LOOP;
END;
/
```
该脚本首先向分类表中插入10条数据，然后通过随机数生成器插入10万个商品，再向用户表插入1000条数据。最后，它生成1000个订单，每笔订单包含100个商品（从商品中随机选择），并随机分配给用户。
![](insert.png)

- ## 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
以下程序包中,包含了对商品表（goods）的查询、插入、更新、删除等操作的存储过程和函数。这些过程和函数可实现商品的增删改查、库存管理等复杂业务逻辑。
```sql
CREATE SEQUENCE goods_seq
  START WITH 1
  INCREMENT BY 1
  NOCACHE;
CREATE OR REPLACE PACKAGE goods_pkg AS
  -- 查询商品表所有记录
  PROCEDURE get_all_goods (p_cursor OUT SYS_REFCURSOR);

  -- 插入一条商品记录
  PROCEDURE insert_goods (
    p_id          IN  NUMBER,
    p_name        IN  VARCHAR2,
    p_price       IN  NUMBER,
    p_stock       IN  NUMBER,
    p_category_id IN  NUMBER
  );

  -- 更新一条商品记录
  PROCEDURE update_goods (
    p_id          IN  NUMBER, 
    p_name        IN  VARCHAR2,
    p_price       IN  NUMBER,
    p_stock       IN  NUMBER,
    p_category_id IN  NUMBER
  );

  -- 删除一条商品记录
  PROCEDURE delete_goods (p_id IN NUMBER);

  -- 查询商品库存数量
  FUNCTION get_stock (p_id IN NUMBER) RETURN NUMBER;
  
  -- 更新商品库存数量
  PROCEDURE update_stock (p_id IN NUMBER, p_stock IN NUMBER);
  
  -- 查询商品价格
  FUNCTION get_price (p_id IN NUMBER) RETURN NUMBER;
END goods_pkg;
/

CREATE OR REPLACE PACKAGE BODY goods_pkg AS
  -- 查询商品表所有记录
  PROCEDURE get_all_goods (p_cursor OUT SYS_REFCURSOR) IS
  BEGIN
    OPEN p_cursor FOR
      SELECT id, name, price, stock, category_id
      FROM goods;
  END;

  -- 插入一条商品记录
  PROCEDURE insert_goods (
    p_id          IN  NUMBER,
    p_name        IN  VARCHAR2,
    p_price       IN  NUMBER,
    p_stock       IN  NUMBER,
    p_category_id IN  NUMBER
  ) IS
  BEGIN
    INSERT INTO goods (id, name, price, stock, category_id)
    VALUES (p_id, p_name, p_price, p_stock, p_category_id);
  END;

  -- 更新一条商品记录
  PROCEDURE update_goods (
    p_id          IN  NUMBER, 
    p_name        IN  VARCHAR2,
    p_price       IN  NUMBER,
    p_stock       IN  NUMBER,
    p_category_id IN  NUMBER
  ) IS
  BEGIN
    UPDATE goods
    SET name = p_name,
        price = p_price,
        stock = p_stock,
        category_id = p_category_id
    WHERE id = p_id;
  END;

  -- 删除一条商品记录
  PROCEDURE delete_goods (p_id IN NUMBER) IS
  BEGIN
    DELETE FROM goods
    WHERE id = p_id;
  END;

  -- 查询商品库存数量
  FUNCTION get_stock (p_id IN NUMBER) RETURN NUMBER IS
    l_stock NUMBER;
  BEGIN
    SELECT stock
    INTO l_stock
    FROM goods
    WHERE id = p_id;
    
    RETURN l_stock;
  END;

  -- 更新商品库存数量
  PROCEDURE update_stock (p_id IN NUMBER, p_stock IN NUMBER) IS
  BEGIN
    UPDATE goods
    SET stock = p_stock
    WHERE id = p_id;
  END;
  
  -- 查询商品价格
  FUNCTION get_price (p_id IN NUMBER) RETURN NUMBER IS
    l_price NUMBER;
  BEGIN
    SELECT price
    INTO l_price
    FROM goods
    WHERE id = p_id;
    
    RETURN l_price;
  END;
END goods_pkg;
/
```
![](package.png)
这个程序包实现了包括增删改查操作、库存管理等业务逻辑。  
以下是对其的调用示例：
- 调用get_all_goods过程查询商品表前10条记录，并使用游标迭代结果集并输出。
```sql
SET SERVEROUTPUT ON;
DECLARE
-- 定义游标变量
l_cursor SYS_REFCURSOR;
-- 定义变量
l_counter INTEGER := 0;
-- 声明游标结果中的变量
id          INTEGER;
name        VARCHAR2(100);
price       NUMBER(10,2);
stock       INTEGER;
category_id INTEGER;
BEGIN
-- 查询前10条商品记录
goods_pkg.get_all_goods(l_cursor);

-- 输出结果
DBMS_OUTPUT.PUT_LINE('ID    NAME    PRICE    STOCK    CATEGORY_ID');
DBMS_OUTPUT.PUT_LINE('--------------------------------------------');
LOOP
FETCH l_cursor INTO id, name, price, stock, category_id;
EXIT WHEN l_cursor%NOTFOUND OR l_counter = 10;
l_counter := l_counter + 1;
DBMS_OUTPUT.PUT_LINE(id || '     ' ||
name || '     ' ||
price || '      ' ||
stock || '       ' ||
category_id);
END LOOP;
END;
```
![](produce.png)
- 调用insert_goods过程插入一条新商品记录。接着，调用update_stock过程更新 id 为 100001的商品库存记录，再次调用get_stock函数查询 id 为 100001 的商品库存记录，并调用get_price函数查询 id 为 100001 的商品价格记录。最后，示例调用delete_goods过程删除 id 为 100001 的商品记录。

```sql
SET SERVEROUTPUT ON;
DECLARE
  -- 定义变量
  stock NUMBER;
  price NUMBER;
BEGIN
  -- 插入一条新商品记录
  goods_pkg.insert_goods(100001,'New Product', 99.99, 100, 1);
  COMMIT; 
  -- 更新一条商品库存记录
  goods_pkg.update_stock(100001, 200);
  COMMIT; 
  -- 查询一条商品库存记录
  stock := goods_pkg.get_stock(100001);
  DBMS_OUTPUT.PUT_LINE('Stock for Product1: ' || stock);

  -- 查询一条商品价格记录
  price := goods_pkg.get_price(100001);
  DBMS_OUTPUT.PUT_LINE('Price for Product 1: ' || price);

  -- 删除一条商品记录
  goods_pkg.delete_goods(100001);
  COMMIT; 
END;
```
- 插入一条新商品记录
![](fun_insert.png)
- 更新一条商品库存记录
![](fun_updata.png)
- 删除一条商品记录
![](fun_delete.png)

- ## 数据库的备份方案。
  RMAN是Oracle提供的备份和恢复工具，可以帮助管理员轻松完成数据库的备份和恢复操作。以下是备份方案的一般步骤：  
-     定义备份策略和存储位置：要选择备份哪些数据以及如何进行备份等，需要先定义备份策略，并确定备份数据存放的位置。这些信息可以在RMAN脚本中指定。
      创建脚本：将备份策略编写为RMAN脚本，以便后续可以快速执行备份操作。
      执行备份：创建好RMAN脚本后，就可以使用RMAN命令行运行该脚本，开始备份数据库。
      定期测试备份：定期测试备份是否可以成功地还原，以确保备份是可靠的
首先启用归档日志模式，并打开已挂载的数据库实例，使其进入可用状态，代码如下：
```sql
$ sqlplus / as sysdba
SQL> SHUTDOWN IMMEDIATE
SQL> STARTUP MOUNT
SQL> ALTER DATABASE ARCHIVELOG;
SQL> ALTER DATABASE OPEN;
```
![](backup1.png)
然后启动RMAN工具以备份整个数据库，包括数据文件、控制文件、参数文件等。
并且LIST BACKUP命令用于列出当前备份集的详细信息，包括备份集的位置、备份类型、备份时间等等，代码如下：
```sql
$ rman target /
RMAN> SHOW ALL;
RMAN> BACKUP DATABASE;
RMAN> LIST BACKUP;
```
如图所示：
![](backup2.png)
![](backup3.png)
![](backup4.png)
![](backup5.png)


## 项目总结：
      本实验项目设计了一套基于商品销售系统的Oracle数据库，对其进行权限及用户分配方案和备份方案进行设计。
      在设计本次项目数据库时，我首先对商品销售系统的数据模型进行设计。
      通过分析系统需求以及业务流程，设计出了名为“goods”、“category”、“user”和“order”的四个表，每个表都有多个字段，并且它们之间存在一些关系：
        
        如外键约束。通过设计表和指定合适的表空间和索引空间，可以优化系统的查询性能。
      
      最后，创建了 RMAN 完全备份脚本。对数据库进行备份，以及安全性的强化。
      本次实验项目不仅帮助对Oracle数据库的设计、管理和备份策略的了解加深，也加深了我对应用软件系统开发的理解和认识。  
      在实验过程中我巩固了分析需求、数据库设计、权限分配方案和备份方案的相关知识，并掌握了一些有用的技巧，例如表空间和索引优化等。
      我相信这些技能和知识将会对我今后的数据管理、数据库设计及应用开发工作带来很大的帮助。
     
