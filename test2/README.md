# 实验2：用户及权限管理
       班级：软件工程4班 学号：202010414422 姓名：杨嘉良

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色local_user，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有local_user的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户user1，给用户分配表空间，设置限额为50M，授予local_user角色。
- 最后测试：用新用户user1连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 1.以system登录到pdborcl，创建角色local_user和用户user1，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE local_user;
GRANT connect,resource,CREATE VIEW TO local_user;
CREATE USER user1 IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER user1 default TABLESPACE "USERS";
ALTER USER user1 QUOTA 50M ON users;
GRANT local_user TO user1;
```
![](step1.png)
```sql
--收回角色
REVOKE local_user FROM user1; 
```

> 语句“ALTER USER user1 QUOTA 50M ON users;”是指授权user1用户访问users表空间，空间限额是50M。

- 2.新用户user1连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus user1/123@pdborcl
SQL> show user;
USER is "user1"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](step2.1.png)
![](step2.2.png)


- 3.用户hr连接到pdborcl，查询user1授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM user1.customers;
Select * from user1.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM user1.customers_view;
NAME
--------------------------------------------------
zhang
wang
```
![](step3.1.png)


> 测试一下用户hr,user1之间的表的共享，只读共享和读写共享都测试一下。  
- 使用user1用户身份测试:
```sql
$ sqlplus user1/123@pdborcl
-- 可以查询表中的数据
SELECT * FROM user1.customers;
-- 可以查询视图中的数据
SELECT * FROM user1.customers_view;
-- 可以向表中插入数据
INSERT INTO user1.customers (id, name) VALUES (3, 'dd');
-- 可以更新表中的数据
UPDATE user1.customers SET name = 'genshit' WHERE id = 3;
-- 可以删除表中的数据
DELETE FROM user1.customers WHERE id = 3;
```
![](test1.png)
> user1用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

- 使用hr用户身份测试:
```sql
-- 只读共享
-- 不可以查询表中的数据
SELECT * FROM user1.customers;
-- 可以查询视图中的数据
SELECT * FROM user1.customers_view;
-- 不可以向表中插入数据
INSERT INTO user1.customers (id, name) VALUES (3, 'hr');
-- 不可以更新表中的数据
UPDATE user1.customers SET name = 'hrrr' WHERE id = 3;
-- 不可以删除表中的数据
DELETE FROM user1.customers WHERE id = 3;

```
![](test2.1.png)
```sql
-- 读写共享
-- 授权，以及授予角色
$ sqlplus system/123@pdborcl
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO local_user;
GRANT SELECT ON customers_view TO local_user;
GRANT local_user TO hr;
```
![](test2.2.1.png)
```sql
$ sqlplus hr/123@pdborcl
-- 可以查询表中的数据
SELECT * FROM user1.customers;
-- 可以查询视图中的数据
SELECT * FROM user1.customers_view;
-- 可以向表中插入数据
INSERT INTO user1.customers (id, name) VALUES (3, 'hr');
-- 可以更新表中的数据
UPDATE user1.customers SET name = 'hrrr' WHERE id = 3;
-- 可以删除表中的数据
DELETE FROM user1.customers WHERE id = 3;
```
![](test2.2.2.png)

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```
![](step3.2.png)

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
![](step3.2.1.png)
- 锁定后，通过system用户登录，alter user user1 unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user hr  account unlock;
```
![](step3.2.2.png)

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色local_user和用户user1。
> 新用户user1使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```
![](step4.png)
- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。
## 总结
       本次实验中，我学习并练习了Oracle数据库的用户及权限管理，也区分学习了只读共享和读写共享，掌握了安全性高、权限清晰、数据共享合理的数据库系统构建方法。同时，明白了在之后数据库操作中，需要注意权限授权和角色管理需要精细、严谨授权，避免不必要的安全和数据问题。

